var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var QuestionSchema= Schema({

       appType: {type: String, required: true, trim: true},

       question: {type: String, required: true, trim: true},
       options: [ Schema({
           id: {type: Number, required: true, trim: true},
           option: {type: String, trim: true}
       })],
       correct: {type:String, required: true, trim: true},
       paperId: {type: String, required: false, trim: true},
       code: {type: String, required: false, trim: true},
       explanation: {type: String, required: false, trim: true},
       deleted: {type: Boolean, default:false},
       category: [{type: String, trim: true}]
   },
   {
       timestamps: true,
       collection: "Question"
   });


module.exports = mongoose.model('Question', QuestionSchema);

