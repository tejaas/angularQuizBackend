var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var AppNameSchema= Schema({

       appName: {type: String, required: true, trim: true,unique:true},
       imageUrl: {type: String, required: false, trim: true}
   },
   {
       timestamps: true,
       collection: "AppName"
   });


module.exports = mongoose.model('AppName', AppNameSchema);


