var app = angular.module('myApp')
    .controller('questionsCtrl', questionsCtrl)

function questionsCtrl($localForage, Question,allApp,Paper,questionArray,$timeout) {
    var questionsCtrl = this;

    questionsCtrl.choice = null;
    questionsCtrl.correctIdLength = null;
    questionsCtrl.questions = {};
    questionsCtrl.questions.category = [];
    questionsCtrl.questions.categoryName=[];
    questionsCtrl.questions.options = [{id: 1}, {id: 2}, {id: 3}, {id: 4},{id:5}];

    questionsCtrl.correctId = function (correct, index) {
        if (!Array.isArray(questionsCtrl.questions.correct)) {
            questionsCtrl.questions.correct = [];
            questionsCtrl.questions.correctMultipleOptions = [];
        }


        if (correct) {
            questionsCtrl.questions.correct.push(index + 1);
            _.each(questionsCtrl.questions.options,function(option){
                  if(option.id==(index+1)){
                      questionsCtrl.questions.correctMultipleOptions.push(option);
                  }
            })
            console.log(questionsCtrl.questions.correct);
            console.log("if");
        }
        else {
            var index = _.indexOf(questionsCtrl.questions.correct, index + 1)
            questionsCtrl.questions.correct.splice(index, 1);
            questionsCtrl.questions.correctMultipleOptions.splice(index,1);
            console.log(questionsCtrl.questions.correct);
            console.log("else");
        }
    }


    //select multiple category

    questionsCtrl.addMultipleCat = function (correct, index,val) {
        console.log(questionsCtrl.arr);
        console.log(val);
           if (correct) {
               questionsCtrl.questions.category.push(val._id);
               questionsCtrl.questions.categoryName.push(val.name);
               console.log(questionsCtrl.questions.category);
               console.log(questionsCtrl.questions.categoryName);
               console.log("if");
           }
           else {
               var index = _.indexOf(questionsCtrl.questions.category, val._id)
               questionsCtrl.questions.category.splice(index, 1);
               console.log(questionsCtrl.questions.category);
               var nameIndex= _.indexOf(questionsCtrl.questions.categoryName,val.name)
               questionsCtrl.questions.categoryName.splice(nameIndex,1);
               console.log(questionsCtrl.questions.categoryName);
               console.log("else");
           }

    }
    questionsCtrl.questionsArray=questionArray.questions;

    questionsCtrl.addQuestion = function () {
        if(questionsCtrl.paperName){
            questionsCtrl.questions.paperId=questionsCtrl.paperName._id;
            questionsCtrl.questions.paperName=questionsCtrl.paperName.name;

        }
        questionsCtrl.questions.options.length=questionsCtrl.optionSize;
        console.log(questionsCtrl.optionSize);
        console.log(questionsCtrl.questions.options);
        console.log(questionsCtrl.questions);

        //convert category array in stringify json
        if (Array.isArray(questionsCtrl.questions.correct)) {
            questionsCtrl.questions.correct = JSON.stringify(questionsCtrl.questions.correct)
        }
        questionsCtrl.questions.appType = questionsCtrl.appData._id;
        //save in localForage
        var key = "paper";

        questionsCtrl.arr=questionsCtrl.questions.category;

        console.log(questionsCtrl.questions.category);
        questionsCtrl.questions.appName=questionsCtrl.appData.appName;
        $localForage.getItem(key).then(function (data) {
            if (data) {
                questionsCtrl.questionsArray = data;
                questionsCtrl.questionsArray.push({
                    question: questionsCtrl.questions

                });
                $localForage.setItem(key, questionsCtrl.questionsArray).then(function () {
                    questionsCtrl.questions = {};
                    //questionsCtrl.questions.paperId=questionsCtrl.paperName._id;
                    questionsCtrl.questions.appType = questionsCtrl.appData._id;
                    questionsCtrl.questions.category = questionsCtrl.arr;
                    questionsCtrl.questions.options = [{id: 1}, {id: 2}, {id: 3}, {id: 4},{id:5}];
                    questionsCtrl.correct1 = null;
                    questionsCtrl.correct2 = null;
                    questionsCtrl.correct3 = null;
                    questionsCtrl.correct4 = null;
                    questionsCtrl.questions.correct=null;
                    questionsCtrl.correctOptions=null;
                    questionsCtrl.category1 = null;
                    questionsCtrl.category2 = null;
                    questionsCtrl.category3 = null;
                    questionsCtrl.category4 = null;
                    questionsCtrl.category5 = null;
                    questionsCtrl.questions.categoryName=[];
                });
                console.log("got", data);
            }

            else {
                questionsCtrl.questionsArray.push({
                    question: questionsCtrl.questions
                });
                $localForage.setItem(key, questionsCtrl.questionsArray).then(function () {
                    questionsCtrl.questions = {};
                    //questionsCtrl.questions.paperId=questionsCtrl.paperName._id;
                    questionsCtrl.questions.appType = questionsCtrl.appData._id;
                    questionsCtrl.questions.category = questionsCtrl.arr ;
                    questionsCtrl.questions.options = [{id: 1}, {id: 2}, {id: 3}, {id: 4},{id:5}];
                    questionsCtrl.correct1 = null;
                    questionsCtrl.correct2 = null;
                    questionsCtrl.correct3 = null;
                    questionsCtrl.correct4 = null;
                    questionsCtrl.questions.correct=null;
                    questionsCtrl.correctOptions=null;
                    questionsCtrl.category1 = null;
                    questionsCtrl.category2 = null;
                    questionsCtrl.category3 = null;
                    questionsCtrl.category4 = null;
                    questionsCtrl.category5 = null;
                    questionsCtrl.questions.categoryName=[];
                });
                console.log("got", data);
            }
        });

    }

    allApp.then(function(data){
        console.log(data);
        questionsCtrl.allAppData=data;
    })
    questionsCtrl.changeValue=function(){
        console.log(questionsCtrl.appData);
        questionsCtrl.papers=[];
        questionsCtrl.topics=[];

        Paper.query({appId:questionsCtrl.appData._id},function(data){
            console.log(data);
            _.each(data,function(d){
                if(!d.type.localeCompare("paper")){
                    questionsCtrl.papers.push(d);
                }
                else{
                    questionsCtrl.topics.push(d);
                }
                console.log(questionsCtrl.topics);
            })

        })
            ,function(err){
            console.log(err);
        }
        questionsCtrl.questions.appName=questionsCtrl.appData.appName;
        console.log( questionsCtrl.questions.appName);
    }
    questionsCtrl.changePaper=function(){
        console.log(questionsCtrl.paperName);
    }
    // questionsCtrl.savePaper();
  //change view between question to review
    questionsCtrl.showQuestion=true;
    questionsCtrl.showReview=false;
    questionsCtrl.changeView=function(){
        questionsCtrl.showReview=!questionsCtrl.showReview;
        questionsCtrl.showQuestion=!questionsCtrl.showQuestion;
    }

    questionsCtrl.changeOptions=function(){
        questionsCtrl.optionSize=questionsCtrl.opLength
        console.log(questionsCtrl.opLength);
        console.log(questionsCtrl.optionSize);
    }
    questionsCtrl.changeCorrectOptions=function(){
        //questionsCtrl.questions.options
        //questionsCtrl.correctOptions;
        _.each(questionsCtrl.questions.options,function(option){
            if(option.id==questionsCtrl.correctOptions){
                questionsCtrl.questions.correctOptions=option;
            }
        })
        questionsCtrl.questions.correct=questionsCtrl.correctOptions;
        console.log(questionsCtrl.questions.correctOptions);
    }
    questionsCtrl.confirmToSave=function(){
        BootstrapDialog.show({
            message: 'Are you sure to save all questions',
            buttons: [{
                label: 'yes',
                // no title as it is optional
                cssClass: 'btn-primary',
                action: function(dialogItself){
                    $localForage.getItem('paper').then(function (data) {
                        if (data != null) {
                            console.log(data);

                            Question.save(data, function (success) {
                                console.log(success);
                                $localForage.clear();
                                questionArray.questions.length=0;
                            } ,function(err)
                            {
                                console.log(err);
                            })
                        }
                    })
                    questionsCtrl.questionsArray=[];
                    console.log(questionsCtrl.appData);
                    console.log(questionsCtrl.appName);
                    dialogItself.close();
                }
            }, {
                label: 'no',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    }
    questionsCtrl.showPopup=function(question){
        var key = "paper";
        BootstrapDialog.show({
            message: 'Are you sure to delete this question',
            buttons: [{
                label: 'yes',
                // no title as it is optional
                cssClass: 'btn-primary',
                action: function(dialogItself){
                    console.log(question);
                    var index= _.indexOf(questionsCtrl.questionsArray,question);
                    $timeout(function()
                    {
                    questionsCtrl.questionsArray=_.reject(questionsCtrl.questionsArray,function(ques)
                    {
                        return ques==question
                    })

                        $localForage.setItem(key, questionsCtrl.questionsArray)
                    },1)
                    console.log(questionsCtrl.questionsArray);


                    dialogItself.close();
                }
            }, {
                label: 'no',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    }
}